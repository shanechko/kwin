# translation of kcmkwinrules.po to Arabic
# translation of kcmkwinrules.po to
# محمد سعد  Mohamed SAAD <metehyi@free.fr>, 2006.
# AbdulAziz AlSharif <a.a-a.s@hotmail.com>, 2007.
# Youssef Chahibi <chahibi@gmail.com>, 2007.
# zayed <zayed.alsaidi@gmail.com>, 2008, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kcmkwinrules\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-07 02:19+0000\n"
"PO-Revision-Date: 2023-06-08 12:05+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 22.12.3\n"

#: kcmrules.cpp:226
#, kde-format
msgid "Copy of %1"
msgstr "نسخة عن %1"

#: kcmrules.cpp:406
#, kde-format
msgid "Application settings for %1"
msgstr "إعدادات التطبيق لـِ %1"

#: kcmrules.cpp:428 rulesmodel.cpp:215
#, kde-format
msgid "Window settings for %1"
msgstr "إعدادات النافذة لـِ %1"

#: optionsmodel.cpp:198
#, kde-format
msgid "Unimportant"
msgstr "غير مهم"

#: optionsmodel.cpp:199
#, kde-format
msgid "Exact Match"
msgstr "تطابق تام"

#: optionsmodel.cpp:200
#, kde-format
msgid "Substring Match"
msgstr "تطابق السلسلة الفرعية"

#: optionsmodel.cpp:201
#, kde-format
msgid "Regular Expression"
msgstr "التعبير النمطي"

#: optionsmodel.cpp:205
#, kde-format
msgid "Apply Initially"
msgstr "طبّق مبدئياً"

#: optionsmodel.cpp:206
#, kde-format
msgid ""
"The window property will be only set to the given value after the window is "
"created.\n"
"No further changes will be affected."
msgstr ""
"ستعين خاصية النافذة إلى القيمة المحددة فقط بعد إنشاء النافذة. \n"
"لن تتأثر أي تغييرات أخرى."

#: optionsmodel.cpp:209
#, kde-format
msgid "Apply Now"
msgstr "طبّق الآن"

#: optionsmodel.cpp:210
#, kde-format
msgid ""
"The window property will be set to the given value immediately and will not "
"be affected later\n"
"(this action will be deleted afterwards)."
msgstr ""
"ستعين خاصية النافذة على القيمة المحددة فورًا ولن تتأثر لاحقًا \n"
"(سيحذف هذا الإجراء بعد ذلك)."

#: optionsmodel.cpp:213
#, kde-format
msgid "Remember"
msgstr "تذكّر"

#: optionsmodel.cpp:214
#, kde-format
msgid ""
"The value of the window property will be remembered and, every time the "
"window is created, the last remembered value will be applied."
msgstr ""
"سيتم تذكر قيمة خاصية النافذة، وفي كل مرة يتم فيها إنشاء النافذة، سيطبق آخر "
"قيمة محفوظة."

#: optionsmodel.cpp:217
#, kde-format
msgid "Do Not Affect"
msgstr "لا يتأثر"

#: optionsmodel.cpp:218
#, kde-format
msgid ""
"The window property will not be affected and therefore the default handling "
"for it will be used.\n"
"Specifying this will block more generic window settings from taking effect."
msgstr ""
"لن تتأثر خاصية النافذة وبالتالي ستستعمل المعالجة الافتراضية لها. \n"
"سيؤدي تحديد هذا إلى منع المزيد من إعدادات النافذة العامة من أن تصبح سارية "
"المفعول."

#: optionsmodel.cpp:221
#, kde-format
msgid "Force"
msgstr "أجبر"

#: optionsmodel.cpp:222
#, kde-format
msgid "The window property will be always forced to the given value."
msgstr "ستفرض القيمة المعطاة لخاصية النافذة بشكل دائم"

#: optionsmodel.cpp:224
#, kde-format
msgid "Force Temporarily"
msgstr "أجبر بشكل مؤقت"

#: optionsmodel.cpp:225
#, kde-format
msgid ""
"The window property will be forced to the given value until it is hidden\n"
"(this action will be deleted after the window is hidden)."
msgstr ""
"ستفرض القيمة المعطاة لخاصية النافذة حتى تختفي\n"
"(هذا الإجراء سيحذف بعد أن تختفي النافذة)"

#: rulesmodel.cpp:218
#, kde-format
msgid "Settings for %1"
msgstr "الإعدادات لِــ %1"

#: rulesmodel.cpp:221
#, kde-format
msgid "New window settings"
msgstr "إعدادات النافذة الجديدة"

#: rulesmodel.cpp:237
#, kde-format
msgid ""
"You have specified the window class as unimportant.\n"
"This means the settings will possibly apply to windows from all "
"applications. If you really want to create a generic setting, it is "
"recommended you at least limit the window types to avoid special window "
"types."
msgstr ""
"لقد حددت أن صنف النافذة غير مهم.\n"
"هذا يعني أنه من الممكن أن تطبق الإعدادات على نوافذ من جميع التطبيقات. إذا "
"كنت تريد فعلا إنشاء إعداد عام ، فإنه من الموصى أن تحدد أنواع النوافذ لمنع "
"أنواع النوافذ الخاصة."

#: rulesmodel.cpp:244
#, kde-format
msgid ""
"Some applications set their own geometry after starting, overriding your "
"initial settings for size and position. To enforce these settings, also "
"force the property \"%1\" to \"Yes\"."
msgstr ""
"تقوم بعض التطبيقات بتعيين الشكل الهندسي الخاص بها بعد البدء ، مما يؤدي إلى "
"تجاوز الإعدادات الأولية للحجم والموضع. لفرض هذه الإعدادات ، قم أيضًا بفرض "
"الخاصية \"‏%1\" على \"نعم\"."

#: rulesmodel.cpp:251
#, kde-format
msgid ""
"Readability may be impaired with extremely low opacity values. At 0%, the "
"window becomes invisible."
msgstr ""
"قد تكون قابلية القراءة ضعيفة مع قيم عتامة منخفضة للغاية. في 0٪، تصبح النافذة "
"غير مرئية."

#: rulesmodel.cpp:382
#, kde-format
msgid "Description"
msgstr "الوصف"

#: rulesmodel.cpp:382 rulesmodel.cpp:390 rulesmodel.cpp:398 rulesmodel.cpp:405
#: rulesmodel.cpp:411 rulesmodel.cpp:419 rulesmodel.cpp:424 rulesmodel.cpp:430
#, kde-format
msgid "Window matching"
msgstr "مطابقة النافذة"

#: rulesmodel.cpp:390
#, kde-format
msgid "Window class (application)"
msgstr "صنف النافذة (التطبيق)"

#: rulesmodel.cpp:398
#, kde-format
msgid "Match whole window class"
msgstr "طابق كل صنف النافذة"

#: rulesmodel.cpp:405
#, kde-format
msgid "Whole window class"
msgstr "كل صنف النافذة"

#: rulesmodel.cpp:411
#, kde-format
msgid "Window types"
msgstr "أنواع النافذة"

#: rulesmodel.cpp:419
#, kde-format
msgid "Window role"
msgstr "دور النافذة"

#: rulesmodel.cpp:424
#, kde-format
msgid "Window title"
msgstr "عنوان النّافذة"

#: rulesmodel.cpp:430
#, kde-format
msgid "Machine (hostname)"
msgstr "الآلة (اسم المضيف)"

#: rulesmodel.cpp:436
#, kde-format
msgid "Position"
msgstr "الموقع"

#: rulesmodel.cpp:436 rulesmodel.cpp:442 rulesmodel.cpp:448 rulesmodel.cpp:453
#: rulesmodel.cpp:461 rulesmodel.cpp:467 rulesmodel.cpp:486 rulesmodel.cpp:502
#: rulesmodel.cpp:507 rulesmodel.cpp:512 rulesmodel.cpp:517 rulesmodel.cpp:522
#: rulesmodel.cpp:531 rulesmodel.cpp:546 rulesmodel.cpp:551 rulesmodel.cpp:556
#, kde-format
msgid "Size & Position"
msgstr "الحجم والموضع"

#: rulesmodel.cpp:442
#, kde-format
msgid "Size"
msgstr "الحجم"

#: rulesmodel.cpp:448
#, kde-format
msgid "Maximized horizontally"
msgstr "مكبّرة أفقياً"

#: rulesmodel.cpp:453
#, kde-format
msgid "Maximized vertically"
msgstr "مكبّرة عمودياً"

#: rulesmodel.cpp:461
#, kde-format
msgid "Virtual Desktop"
msgstr "كلّ أسطح المكتب الافتراضية"

#: rulesmodel.cpp:467
#, kde-format
msgid "Virtual Desktops"
msgstr "أسطح المكتب الافتراضية"

#: rulesmodel.cpp:486
#, kde-format
msgid "Activities"
msgstr "الأنشطة"

#: rulesmodel.cpp:502
#, kde-format
msgid "Screen"
msgstr "الشّاشة"

#: rulesmodel.cpp:507
#, kde-format
msgid "Fullscreen"
msgstr "ملء الشّاشة"

#: rulesmodel.cpp:512
#, kde-format
msgid "Minimized"
msgstr "مصغرة"

#: rulesmodel.cpp:517
#, kde-format
msgid "Shaded"
msgstr "مظللة"

#: rulesmodel.cpp:522
#, kde-format
msgid "Initial placement"
msgstr "الموضع المبدئي"

#: rulesmodel.cpp:531
#, kde-format
msgid "Ignore requested geometry"
msgstr "تجاهل طلب الأبعاد"

#: rulesmodel.cpp:534
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Some applications can set their own geometry, overriding the window manager "
"preferences. Setting this property overrides their placement requests.<nl/"
"><nl/>This affects <interface>Size</interface> and <interface>Position</"
"interface> but not <interface>Maximized</interface> or "
"<interface>Fullscreen</interface> states.<nl/><nl/>Note that the position "
"can also be used to map to a different <interface>Screen</interface>"
msgstr ""
"يمكن لبعض التطبيقات أن تحدد أبعادها بنفسها بطريقة تتجاوز تفضيلات مدير "
"النوافذ. هذه الخاصية تسمح بتجاوز تلك الإمكانية.<nl/><nl/> تؤثر هذه  على "
"<interface>الحجم</interface> و <interface>الموضع</interface> ولكن لا تؤثر "
"على حالتي <interface>التكبير</interface> أو <interface>ملء الشاشة</"
"interface>.<nl/><nl/> لاحظ أن الموضع يمكن يستخدم في <interface>شاشة</"
"interface> أخرى."

#: rulesmodel.cpp:546
#, kde-format
msgid "Minimum Size"
msgstr "الحجم الأدنى:"

#: rulesmodel.cpp:551
#, kde-format
msgid "Maximum Size"
msgstr "الحجم الأقصى:"

#: rulesmodel.cpp:556
#, kde-format
msgid "Obey geometry restrictions"
msgstr "التزم بقيود الأبعاد"

#: rulesmodel.cpp:558
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Some apps like video players or terminals can ask KWin to constrain them to "
"certain aspect ratios or only grow by values larger than the dimensions of "
"one character. Use this property to ignore such restrictions and allow those "
"windows to be resized to arbitrary sizes.<nl/><nl/>This can be helpful for "
"windows that can't quite fit the full screen area when maximized."
msgstr ""
"بعض التطبيقات مثل الأجهزة الطرفية أو مشغلات الفيديو يمكن أن تطلب من كوين "
"الاحتفاظ بنسبة عرض إلى ارتفاع معينة أو تكبر فقط بقيم أكبر من أبعاد محرف "
"واحد. استخدم هذه الخاصية لتجاهل مثل هذه القيود بحيث يمكن تغيير أبعاد النوافذ "
"لأي قيم.<nl/><nl/> قد يكون هذا مفيداً للنوافذ التي لا تستطيع أن تتلاءم مع "
"مساحة الشاشة الكاملة عند تكبيرها."

#: rulesmodel.cpp:569
#, kde-format
msgid "Keep above other windows"
msgstr "أبقها أعلى النوافذ الأخرى"

#: rulesmodel.cpp:569 rulesmodel.cpp:574 rulesmodel.cpp:579 rulesmodel.cpp:585
#: rulesmodel.cpp:591 rulesmodel.cpp:597
#, kde-format
msgid "Arrangement & Access"
msgstr "الترتيب والوصول"

#: rulesmodel.cpp:574
#, kde-format
msgid "Keep below other windows"
msgstr "أبقها أسفل النوافذ الأخرى"

#: rulesmodel.cpp:579
#, kde-format
msgid "Skip taskbar"
msgstr "تخطى شريط المهام"

#: rulesmodel.cpp:581
#, kde-format
msgctxt "@info:tooltip"
msgid "Controls whether or not the window appears in the Task Manager."
msgstr "يتحكم في ما إذا كان يجب أن تظهر النافذة في مدير سطح المهام أم لا."

#: rulesmodel.cpp:585
#, kde-format
msgid "Skip pager"
msgstr "تخطّى المنادي"

#: rulesmodel.cpp:587
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Controls whether or not the window appears in the Virtual Desktop manager."
msgstr ""
"يتحكم في ما إذا كان يجب أن تظهر النافذة في مدير سطح المكتب الافتراضي أم لا."

#: rulesmodel.cpp:591
#, kde-format
msgid "Skip switcher"
msgstr "تخطّى مبدل المهام"

#: rulesmodel.cpp:593
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Controls whether or not the window appears in the <shortcut>Alt+Tab</"
"shortcut> window list."
msgstr ""
"يتحكم في ما إذا كان يجب أن تظهر النافذة في قائمة النوافذ <shortcut>Alt+Tab</"
"shortcut> أم لا."

#: rulesmodel.cpp:597
#, kde-format
msgid "Shortcut"
msgstr "الاختصار"

#: rulesmodel.cpp:603
#, kde-format
msgid "No titlebar and frame"
msgstr "بدون شريط العنوان والإطار"

#: rulesmodel.cpp:603 rulesmodel.cpp:608 rulesmodel.cpp:614 rulesmodel.cpp:619
#: rulesmodel.cpp:625 rulesmodel.cpp:652 rulesmodel.cpp:680 rulesmodel.cpp:686
#: rulesmodel.cpp:698 rulesmodel.cpp:703 rulesmodel.cpp:709 rulesmodel.cpp:714
#, kde-format
msgid "Appearance & Fixes"
msgstr "المظهر والإصلاحات"

#: rulesmodel.cpp:608
#, kde-format
msgid "Titlebar color scheme"
msgstr "مخطط لون شريط العنوان"

#: rulesmodel.cpp:614
#, kde-format
msgid "Active opacity"
msgstr "التعتيم نشط"

#: rulesmodel.cpp:619
#, kde-format
msgid "Inactive opacity"
msgstr "التعتيم خامل"

#: rulesmodel.cpp:625
#, kde-format
msgid "Focus stealing prevention"
msgstr "منع سرقة التركيز"

#: rulesmodel.cpp:627
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"KWin tries to prevent windows that were opened without direct user action "
"from raising themselves and taking focus while you're currently interacting "
"with another window. This property can be used to change the level of focus "
"stealing prevention applied to individual windows and apps.<nl/><nl/>Here's "
"what will happen to a window opened without your direct action at each level "
"of focus stealing prevention:<nl/><list><item><emphasis strong='true'>None:</"
"emphasis> The window will be raised and focused.</item><item><emphasis "
"strong='true'>Low:</emphasis> Focus stealing prevention will be applied, but "
"in the case of a situation KWin considers ambiguous, the window will be "
"raised and focused.</item><item><emphasis strong='true'>Normal:</emphasis> "
"Focus stealing prevention will be applied, but  in the case of a situation "
"KWin considers ambiguous, the window will <emphasis>not</emphasis> be raised "
"and focused.</item><item><emphasis strong='true'>High:</emphasis> The window "
"will only be raised and focused if it belongs to the same app as the "
"currently-focused window.</item><item><emphasis strong='true'>Extreme:</"
"emphasis> The window will never be raised and focused.</item></list>"
msgstr ""
"حاول كوين منع النوافذ المفتوحة بدون تدخل مباشر من المستخدم من الارتفاع "
"والتركيز بينما أنت تتفاعل مع نافذة أخرى في نفس الوقت. يمكن استخدام هذه "
"الخاصية لتغيير مستوى مانع سرقة التركيز المطبق على النوافذ والتطبيقات الفردية."
"<nl/><nl/>هنا ما سيحدث للنافذة التي فُتحت دون تدخل مباشر منك عند كل مستوى "
"لمانع سرقة التركيز:<nl/><list><item><emphasis strong='true'>لا شيء:</"
"emphasis> سترفع النافذة ويركز عليها.</item><item><emphasis "
"strong='true'>منخفض:</emphasis> سيطبق مانع سرقة التركيز، ولكن في حالة وجود "
"حالة غامضة سترفع النافذة ويركز عليها.</item><item><emphasis "
"strong='true'>عادي:</emphasis> سيطبق مانع سرقة التركيز، ولكن في حالة وجود "
"حالة غامضة، <emphasis>فلن</emphasis> ترفع النافذة ويركز عليها.</"
"item><item><emphasis strong='true'>عالي:</emphasis>  سترفع النافذة ويركز "
"عليها فقط إذا كانت تنتمي إلى نفس التطبيق الذي عليه التركيز  حاليًا.</"
"item><item><emphasis strong='true'>قصوى:</emphasis>  لن ترفع النافذة ويركز "
"عليها.</item></list>"

#: rulesmodel.cpp:652
#, kde-format
msgid "Focus protection"
msgstr "حماية التركيز"

#: rulesmodel.cpp:654
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"This property controls the focus protection level of the currently active "
"window. It is used to override the focus stealing prevention applied to new "
"windows that are opened without your direct action.<nl/><nl/>Here's what "
"happens to new windows that are opened without your direct action at each "
"level of focus protection while the window with this property applied to it "
"has focus:<nl/><list><item><emphasis strong='true'>None</emphasis>: Newly-"
"opened windows always raise themselves and take focus.</item><item><emphasis "
"strong='true'>Low:</emphasis> Focus stealing prevention will be applied to "
"the newly-opened window, but in the case of a situation KWin considers "
"ambiguous, the window will be raised and focused.</item><item><emphasis "
"strong='true'>Normal:</emphasis> Focus stealing prevention will be applied "
"to the newly-opened window, but in the case of a situation KWin considers "
"ambiguous, the window will <emphasis>not</emphasis> be raised and focused.</"
"item><item><emphasis strong='true'>High:</emphasis> Newly-opened windows "
"will only raise themselves and take focus if they belongs to the same app as "
"the currently-focused window.</item><item><emphasis strong='true'>Extreme:</"
"emphasis> Newly-opened windows never raise themselves and take focus.</"
"item></list>"
msgstr ""
"تتحكم هذه الخاصية في مستوى حماية التركيز للنافذة النشطة حاليًا. تستخدم هذه "
"الخاصية لتجاوز منع سرقة التركيز المطبق على النوافذ الجديدة التي تفتح دون "
"إجراء مباشر من جانبك.<nl/> <nl/>إليك ما سيحدث للنوافذ الجديدة التي تفتح دون "
"إجراء من قبلك في كل مستوى من مستويات حماية التركيز بينما النافذة التي يطبق "
"عليها هذه الخاصية في وضع التركيز:<nl/><list><item> <focus strong = 'true'> "
"لا شيء </emphasis>: النوافذ المفتوحة حديثًا ترفع نفسها دائمًا وتركز على نفسها. "
"</item><item> <focus strong = 'true'> منخفض: </emphasis> سيطبق منع سرقة "
"التركيز على النافذة المفتوحة حديثًا، ولكن في حالة وجود موقف يعتبره كوين "
"غامضًا، سترفع النافذة ويركز عليها. </item><item> <focus strong = 'true'> "
"عادي: </emphasis> سيطبق منع سرقة التركيز على النافذة المفتوحة حديثًا، ولكن في "
"حالة وجود موقف يعتبره كوين غامضًا، فإن النافذة <emphasis> لن </emphasis>ترفع "
"ولن يركز عليها. </item><item> <focus strong = 'true'> عالي: </emphasis> لن "
"ترفع النوافذ المفتوحة حديثًا إلا إذا كانت تنتمي إلى نفس التطبيق مثل النافذة "
"المركزة حاليًا. </item><item> <focus strong = 'true'> قصوى: </emphasis> "
"النوافذ المفتوحة حديثًا لا ترفع نفسها أبدًا ولا يركز عليها. </item></list>"

#: rulesmodel.cpp:680
#, kde-format
msgid "Accept focus"
msgstr "أقبل التركيز"

#: rulesmodel.cpp:682
#, kde-format
msgid "Controls whether or not the window becomes focused when clicked."
msgstr ""
"يتحكم في ما إذا كان يجب أن تصبح النافذة في وضع التركيز عند النقر عليها أم لا."

#: rulesmodel.cpp:686
#, kde-format
msgid "Ignore global shortcuts"
msgstr "تجاهل الاختصارات العامة"

#: rulesmodel.cpp:688
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"Use this property to prevent global keyboard shortcuts from working while "
"the window is focused. This can be useful for apps like emulators or virtual "
"machines that handle some of the same shortcuts themselves.<nl/><nl/>Note "
"that you won't be able to <shortcut>Alt+Tab</shortcut> out of the window or "
"use any other global shortcuts such as <shortcut>Alt+Space</shortcut> to "
"activate KRunner."
msgstr ""
"استخدم هذه الخاصية لمنع اختصارات لوحة المفاتيح العامة من العمل عندما تكون "
"النافذة في وضع التركيز. يمكن أن يكون هذا مفيدًا لتطبيقات مثل المحاكيات أو "
"الأجهزة الافتراضية التي تتعامل مع بعض الاختصارات نفسها بنفسها. <nl/><nl/"
">لاحظ أنك لن تتمكن من استخدام <shortcut> Alt+Tab </shortcut> من نافذة أو "
"استخدم أي اختصارات عامة أخرى مثل <shortcut> Alt+Space </shortcut> لتنشيط "
"مشغلك."

#: rulesmodel.cpp:698
#, kde-format
msgid "Closeable"
msgstr "قابلة للإغلاق"

#: rulesmodel.cpp:703
#, kde-format
msgid "Set window type"
msgstr "حدد نوع النافذة"

#: rulesmodel.cpp:709
#, kde-format
msgid "Desktop file name"
msgstr "اسم ملف سطح المكتب"

#: rulesmodel.cpp:714
#, kde-format
msgid "Block compositing"
msgstr "التركيب الكتلي"

#: rulesmodel.cpp:766
#, kde-format
msgid "Window class not available"
msgstr "صنف النافذة غير متوفر"

#: rulesmodel.cpp:767
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application is not providing a class for the window, so KWin cannot use "
"it to match and apply any rules. If you still want to apply some rules to "
"it, try to match other properties like the window title instead.<nl/><nl/"
">Please consider reporting this bug to the application's developers."
msgstr ""
"لا يوفر هذا التطبيق صنف للنافذة، لذلك لا يمكن لـكوين استخدامه لمطابقة أي "
"قواعد وتطبيقها. إذا كنت لا تزال ترغب في تطبيق بعض القواعد عليها، فحاول "
"مطابقة الخصائص الأخرى مثل عنوان النافذة بدلاً من ذلك. <nl/> <nl/> يرجى "
"التفكير في الإبلاغ عن هذا الخطأ لمطوري التطبيق."

#: rulesmodel.cpp:801
#, kde-format
msgid "All Window Types"
msgstr "جميع أنواع النافذة"

#: rulesmodel.cpp:802
#, kde-format
msgid "Normal Window"
msgstr "نافذة عادية"

#: rulesmodel.cpp:803
#, kde-format
msgid "Dialog Window"
msgstr "نافذة الحوار"

#: rulesmodel.cpp:804
#, kde-format
msgid "Utility Window"
msgstr "أداة النافذة"

#: rulesmodel.cpp:805
#, kde-format
msgid "Dock (panel)"
msgstr "إرساء (اللوحة)"

#: rulesmodel.cpp:806
#, kde-format
msgid "Toolbar"
msgstr "شريط الأدوات"

#: rulesmodel.cpp:807
#, kde-format
msgid "Torn-Off Menu"
msgstr "قائمة ممزقة"

#: rulesmodel.cpp:808
#, kde-format
msgid "Splash Screen"
msgstr "شاشة البداية"

#: rulesmodel.cpp:809
#, kde-format
msgid "Desktop"
msgstr "سطح المكتب"

#. i18n("Unmanaged Window")},  deprecated
#: rulesmodel.cpp:811
#, kde-format
msgid "Standalone Menubar"
msgstr "شريط أدوات مستقل"

#: rulesmodel.cpp:812
#, kde-format
msgid "On Screen Display"
msgstr "على شاشة العرض"

#: rulesmodel.cpp:822
#, kde-format
msgid "All Desktops"
msgstr "كلّ أسطح المكتب"

#: rulesmodel.cpp:824
#, kde-format
msgctxt "@info:tooltip in the virtual desktop list"
msgid "Make the window available on all desktops"
msgstr "اجعل النافذة متوفرة على كلّ أسطح المكتب"

#: rulesmodel.cpp:843
#, kde-format
msgid "All Activities"
msgstr "كلّ الأنشطة"

#: rulesmodel.cpp:845
#, kde-format
msgctxt "@info:tooltip in the activity list"
msgid "Make the window available on all activities"
msgstr "اجعل النافذة متوفرة على كلّ الأنشطة"

#: rulesmodel.cpp:866
#, kde-format
msgid "Default"
msgstr "افتراضي"

#: rulesmodel.cpp:867
#, kde-format
msgid "No Placement"
msgstr "موضع غير محدد"

#: rulesmodel.cpp:868
#, kde-format
msgid "Minimal Overlapping"
msgstr "الحد الأدنى من التراكب"

#: rulesmodel.cpp:869
#, kde-format
msgid "Maximized"
msgstr "مكبرة"

#: rulesmodel.cpp:870
#, kde-format
msgid "Centered"
msgstr "موسّطة"

#: rulesmodel.cpp:871
#, kde-format
msgid "Random"
msgstr "عشوائي"

#: rulesmodel.cpp:872
#, kde-format
msgid "In Top-Left Corner"
msgstr "في زاوِيَة أعلى اليسار"

#: rulesmodel.cpp:873
#, kde-format
msgid "Under Mouse"
msgstr "تحت الفأرة"

#: rulesmodel.cpp:874
#, kde-format
msgid "On Main Window"
msgstr "على النافذة الرئيسية"

#: rulesmodel.cpp:881
#, kde-format
msgid "None"
msgstr "لا شيء"

#: rulesmodel.cpp:882
#, kde-format
msgid "Low"
msgstr "منخفض"

#: rulesmodel.cpp:883
#, kde-format
msgid "Normal"
msgstr "عادي"

#: rulesmodel.cpp:884
#, kde-format
msgid "High"
msgstr "عالي"

#: rulesmodel.cpp:885
#, kde-format
msgid "Extreme"
msgstr "قصوى"

#: rulesmodel.cpp:928
#, kde-format
msgid "Unmanaged window"
msgstr "نافذة غير مدارة"

#: rulesmodel.cpp:929
#, kde-format
msgid "Could not detect window properties. The window is not managed by KWin."
msgstr "لا يمكن اكتشاف خواص النافذة. لا تدار النافذة بواسطة كوين."

#: ui/FileDialogLoader.qml:15
#, kde-format
msgid "Select File"
msgstr "اختر ملفًّا"

#: ui/FileDialogLoader.qml:27
#, kde-format
msgid "KWin Rules (*.kwinrule)"
msgstr "قواعد كوين (‏*.kwinrule)"

#: ui/main.qml:62
#, kde-format
msgid "No rules for specific windows are currently set"
msgstr "لا توجد قواعد للنوافذ المحددة حاليا"

#: ui/main.qml:63
#, kde-kuit-format
msgctxt "@info"
msgid "Click the <interface>Add New…</interface> button below to add some"
msgstr "انقر على زر <interface>أضف أيضا…</interface> في الأسفل لتضيف بعضها"

#: ui/main.qml:71
#, kde-format
msgid "Select the rules to export"
msgstr "اختر قواعد لتصدرها"

#: ui/main.qml:75
#, kde-format
msgid "Unselect All"
msgstr "أزِل تحديد الكلّ"

#: ui/main.qml:75
#, kde-format
msgid "Select All"
msgstr "حدّد الكلّ"

#: ui/main.qml:89
#, kde-format
msgid "Save Rules"
msgstr "احفظ القواعد"

#: ui/main.qml:100
#, kde-format
msgid "Add New…"
msgstr "أضف أيضاً…"

#: ui/main.qml:111
#, kde-format
msgid "Import…"
msgstr "استورد…"

#: ui/main.qml:119
#, kde-format
msgid "Cancel Export"
msgstr "ألغ التصدير"

#: ui/main.qml:119
#, kde-format
msgid "Export…"
msgstr "صدّر…"

#: ui/main.qml:209
#, kde-format
msgid "Edit"
msgstr "حرّر"

#: ui/main.qml:218
#, kde-format
msgid "Duplicate"
msgstr "كرّر"

#: ui/main.qml:227
#, kde-format
msgid "Delete"
msgstr "احذف"

#: ui/main.qml:240
#, kde-format
msgid "Import Rules"
msgstr "استورد قواعد"

#: ui/main.qml:252
#, kde-format
msgid "Export Rules"
msgstr "صدر قواعد"

#: ui/OptionsComboBox.qml:35
#, kde-format
msgid "None selected"
msgstr "غير المحدد"

#: ui/OptionsComboBox.qml:41
#, kde-format
msgid "All selected"
msgstr "كل المحدد"

#: ui/OptionsComboBox.qml:43
#, kde-format
msgid "%1 selected"
msgid_plural "%1 selected"
msgstr[0] "محدّد"
msgstr[1] "1 محدّد"
msgstr[2] "2 محدّدين"
msgstr[3] "%1 محدّدات"
msgstr[4] "%1 محدّداً"
msgstr[5] "%1 محدّد"

#: ui/RulesEditor.qml:63
#, kde-format
msgid "No window properties changed"
msgstr "لم يغير أي من خواص النافذة"

#: ui/RulesEditor.qml:64
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Click the <interface>Add Property...</interface> button below to add some "
"window properties that will be affected by the rule"
msgstr ""
"انقر على زر <interface>أضف خاصية...</interface> في الأسفل لتضيف بعضاً من "
"خصائص النافذة التي ستكون مشمولة بالقاعدة"

#: ui/RulesEditor.qml:85
#, kde-format
msgid "Close"
msgstr "أغلق"

#: ui/RulesEditor.qml:85
#, kde-format
msgid "Add Property..."
msgstr "أضف خاصية..."

#: ui/RulesEditor.qml:98
#, kde-format
msgid "Detect Window Properties"
msgstr "اكتشف خاصيّات النّافذة"

#: ui/RulesEditor.qml:114 ui/RulesEditor.qml:121
#, kde-format
msgid "Instantly"
msgstr "فوري"

#: ui/RulesEditor.qml:115 ui/RulesEditor.qml:126
#, kde-format
msgid "After %1 second"
msgid_plural "After %1 seconds"
msgstr[0] "بعد %1 ثانية"
msgstr[1] "بعد ثانية"
msgstr[2] "بعد ثانيتين"
msgstr[3] "بعد %1 ثواني"
msgstr[4] "بعد %1 ثانية"
msgstr[5] "بعد %1 ثانية"

#: ui/RulesEditor.qml:175
#, kde-format
msgid "Add property to the rule"
msgstr "أضف خاصية للقاعدة"

#: ui/RulesEditor.qml:276 ui/ValueEditor.qml:54
#, kde-format
msgid "Yes"
msgstr "نعم"

#: ui/RulesEditor.qml:276 ui/ValueEditor.qml:60
#, kde-format
msgid "No"
msgstr "لا"

#: ui/RulesEditor.qml:278 ui/ValueEditor.qml:168 ui/ValueEditor.qml:175
#, kde-format
msgid "%1 %"
msgstr "%1 ٪"

#: ui/RulesEditor.qml:280
#, kde-format
msgctxt "Coordinates (x, y)"
msgid "(%1, %2)"
msgstr "(%1، %2)"

#: ui/RulesEditor.qml:282
#, kde-format
msgctxt "Size (width, height)"
msgid "(%1, %2)"
msgstr "(%1، %2)"

#: ui/ValueEditor.qml:203
#, kde-format
msgctxt "(x, y) coordinates separator in size/position"
msgid "x"
msgstr "×"

#~ msgid "Window shall (not) appear in the taskbar."
#~ msgstr "لا/يجب أن تظهر النافذة في شريط المهام."

#~ msgid "Window shall (not) appear in the manager for virtual desktops"
#~ msgstr "لا/يجب أن تظهر النافذة في مدير أسطح المكتب الافتراضية"

#~ msgid "Window shall (not) appear in the Alt+Tab list"
#~ msgstr "لا/يجب أن تظهر النافذة قائمة مبدل المهام Alt+Tab"

#, fuzzy
#~| msgid ""
#~| "KWin tries to prevent windows from taking the focus\n"
#~| "(\"activate\") while you're working in another window,\n"
#~| "but this may sometimes fail or superact.\n"
#~| "\"None\" will unconditionally allow this window to get the focus while\n"
#~| "\"Extreme\" will completely prevent it from taking the focus."
#~ msgid ""
#~ "KWin tries to prevent windows from taking the focus (\"activate\") while "
#~ "you're working in another window, but this may sometimes fail or "
#~ "superact. \"None\" will unconditionally allow this window to get the "
#~ "focus while \"Extreme\" will completely prevent it from taking the focus."
#~ msgstr ""
#~ "يحاول كوين منع الإطارات من الاستيلاء على التركيز \n"
#~ "(\"تنشيط\") أثناء عملك في نافذة أخرى ، \n"
#~ "لكن هذا قد يفشل أحيانًا أو  يتم تجاوزه. \n"
#~ "\"بلا\" سيسمح لهذه النافذة دون قيد أو شرط بالحصول على التركيز  \n"
#~ "بينما \"القصوى\" ستمنعه تمامًا من الاستيلاء على التركيز."

#, fuzzy
#~| msgid ""
#~| "This controls the focus protection of the currently active window.\n"
#~| "None will always give the focus away,\n"
#~| "Extreme will keep it.\n"
#~| "Otherwise it's interleaved with the stealing prevention\n"
#~| "assigned to the window that wants the focus."
#~ msgid ""
#~ "This controls the focus protection of the currently active window. None "
#~ "will always give the focus away, Extreme will keep it. Otherwise it's "
#~ "interleaved with the stealing prevention assigned to the window that "
#~ "wants the focus."
#~ msgstr ""
#~ "يتحكم هذا في حماية التركيز للنافذة النشطة حاليا. \n"
#~ "ستقوم \"بلا\" بإعطاء التركيز دائمًا، \n"
#~ "و \"القصوى\" ستحتفظ به. \n"
#~ "وإلا فإنه يتداخل مع منع السرقة \n"
#~ "المخصصة للنافذة التي تريد التركيز."

#, fuzzy
#~| msgid ""
#~| "Windows may prevent to get the focus (activate) when being clicked.\n"
#~| "On the other hand you might wish to prevent a window\n"
#~| "from getting focused on a mouse click."
#~ msgid ""
#~ "Windows may prevent to get the focus (activate) when being clicked. On "
#~ "the other hand you might wish to prevent a window from getting focused on "
#~ "a mouse click."
#~ msgstr ""
#~ "قد تمتنع النوافذ من الحصول على التركيز (التنشيط) عند النقر فوقها. \n"
#~ "من ناحية أخرى ، قد ترغب في منع نافذة \n"
#~ "من التركيز عند النقر بالفأرة."

#, fuzzy
#~| msgid ""
#~| "When used, a window will receive\n"
#~| "all keyboard inputs while it is active, including Alt+Tab etc.\n"
#~| "This is especially interesting for emulators or virtual machines.\n"
#~| "\n"
#~| "Be warned:\n"
#~| "you won't be able to Alt+Tab out of the window\n"
#~| "nor use any other global shortcut (such as Alt+F2 to show KRunner)\n"
#~| "while it's active!"
#~ msgid ""
#~ "When used, a window will receive all keyboard inputs while it is active, "
#~ "including Alt+Tab etc. This is especially interesting for emulators or "
#~ "virtual machines. \n"
#~ "Be warned: you won't be able to Alt+Tab out of the window nor use any "
#~ "other global shortcut (such as Alt+F2 to show KRunner) while it's active!"
#~ msgstr ""
#~ "عند الاستخدام ستتلقى نافذة \n"
#~ "كافة إدخالات لوحة المفاتيح أثناء نشاطها ، بما في ذلك Alt + Tab إلخ. \n"
#~ "قد يكون هذا مهم بشكل خاص للمحاكيات أو الآلات التخيلية. \n"
#~ "\n"
#~ "كن حذرًا: \n"
#~ "لن تتمكن من استخدام  Alt + Tab خارج النافذة \n"
#~ "أو استخدام أي اختصار عام آخر (مثل Alt + F2 لإظهار مشغلك) \n"
#~ "عندما تكون النافذة نشطة!"

#~ msgid ""
#~ "Windows can ask to appear in a certain position.\n"
#~ "By default this overrides the placement strategy\n"
#~ "what might be nasty if the client abuses the feature\n"
#~ "to unconditionally popup in the middle of your screen."
#~ msgstr ""
#~ "يمكن أن تطلب النوافذ الظهور في موضع معين. \n"
#~ "يؤدي هذا بشكل افتراضي إلى تجاوز استراتيجية الوضع \n"
#~ "ما قد يكون سيئًا إذا أساء العميل استخدام الميزة \n"
#~ "ليظهر النافذة دون قيد أو شرط في منتصف الشاشة."

#~ msgid "KWinRules KCM launcher"
#~ msgstr "مطلق وحدة تحكم بقواد كوين"

#~ msgid "KWin id of the window for special window settings."
#~ msgstr "معرف كوين للنافذة لإعدادات الخاصة بالنافذة."

#~ msgid "Whether the settings should affect all windows of the application."
#~ msgstr "إذا كانت الإعدادات ستوثر على كلّ نوافذ التطبيق"

#~ msgid "This helper utility is not supposed to be called directly."
#~ msgstr "أداة المساعدة  هذه لا تدعم الاستدعاء مباشرة"

#~ msgctxt "Window caption for the application wide rules dialog"
#~ msgid "Edit Application-Specific Settings"
#~ msgstr "حرّر الإعدادات الخاصة بالتطبيق"

#~ msgid "Edit Window-Specific Settings"
#~ msgstr "حرّر الإعدادات الخاصة بالنافذة"

#~ msgid ""
#~ "<p><h1>Window-specific Settings</h1> Here you can customize window "
#~ "settings specifically only for some windows.</p> <p>Please note that this "
#~ "configuration will not take effect if you do not use KWin as your window "
#~ "manager. If you do use a different window manager, please refer to its "
#~ "documentation for how to customize window behavior.</p>"
#~ msgstr ""
#~ "<p><h1>إعدادات خاصة بالنافذة</h1> هنا يمكنك تخصيص إعدادات النافذة خصوصا "
#~ "لبعض النوافذ فقط.</p> <p>الرجاء ملاحظة أن هذا الضبط لن يفعل إذا اختر غير "
#~ "كوِن كمدير للنوافذ.إذا كنت تستخدم مدير نوافذ مختلف ، فالرجاء الرجوع إلى "
#~ "وثائقهلكيفية تخصيص سلوك النافذة.</p>"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Mohamed SAAD محمد سعد,زايد السعيدي"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "metehyi@free.fr,zayed.alsaidi@gmail.com"

#~ msgid "Window Rules"
#~ msgstr "قواعد النوافذ"

#~ msgid "Ismael Asensio"
#~ msgstr "Ismael Asensio"

#~ msgid "Author"
#~ msgstr "المؤلف"

#~ msgid "Error"
#~ msgstr "خطأ"

#~ msgid "Cascaded"
#~ msgstr "متتالي"

#, fuzzy
#~| msgid "Keep &above"
#~ msgid "Keep above"
#~ msgstr "أبقهِ أ&على"

#, fuzzy
#~| msgid "Keep &below"
#~ msgid "Keep below"
#~ msgstr "أبقهِ أ&سفل"

#~ msgid "KWin"
#~ msgstr "كِون"

#~ msgid "KWin helper utility"
#~ msgstr "أداة مساعدة كِون"

#, fuzzy
#~| msgid "&Detect Window Properties"
#~ msgid "Select properties"
#~ msgstr "ا&كشف خصائص النافذة"

#~ msgid "Override Type"
#~ msgstr "نوع التجاوز"

#~ msgid "Unknown - will be treated as Normal Window"
#~ msgstr "مجهول - سوف تعامل كنافذة عادية"

#~ msgid "Information About Selected Window"
#~ msgstr "معلومات حول النافذة المنتقاة"

#~ msgid "Class:"
#~ msgstr "الصنف:"

#~ msgid "Role:"
#~ msgstr "الدور:"

#~ msgid "Type:"
#~ msgstr "النوع:"

#~ msgid "Title:"
#~ msgstr "العنوان:"

#~ msgid "Machine:"
#~ msgstr "الآلة:"

#~ msgid "&Single Shortcut"
#~ msgstr "ا&ختصار فردي"

#~ msgid "C&lear"
#~ msgstr "ا&مح"

#~ msgid "Window-Specific Settings Configuration Module"
#~ msgstr "نافذة-وحدة تشكيل الإعدادات الخاصة"

#~ msgid "(c) 2004 KWin and KControl Authors"
#~ msgstr "(c) 2004 KWin and KControl Authors"

#~ msgid "Lubos Lunak"
#~ msgstr "Lubos Lunak"

#~ msgid "Remember settings separately for every window"
#~ msgstr "تذكر الإعدادات المنفردة لكلّ نافذة"

#~ msgid "Show internal settings for remembering"
#~ msgstr "أظهر الإعدادات الداخلية للتذكير"

#~ msgid "Internal setting for remembering"
#~ msgstr "الإعدادات الداخلية للتذكير"

#~ msgid "&Modify..."
#~ msgstr "&عدّل..."

#~ msgid "Move &Up"
#~ msgstr "حرّك للأ&على"

#~ msgid "Move &Down"
#~ msgstr "حرّك للأ&سفل"

#~ msgid ""
#~ "Enable this checkbox to alter this window property for the specified "
#~ "window(s)."
#~ msgstr "مكّن خانة التأشير هذه لتغيير هذه الخاصية بالنسبة للنافذة المحدّدة."

#~ msgid "Unnamed entry"
#~ msgstr "مدخلة غير مسماة"

#~ msgid ""
#~ "This configuration dialog allows altering settings only for the selected "
#~ "window or application. Find the setting you want to affect, enable the "
#~ "setting using the checkbox, select in what way the setting should be "
#~ "affected and to which value."
#~ msgstr ""
#~ "يسمح لك حوار الضبط هذا بتغير إعدادات النافذة أو التطبيق المنتقى فقط . "
#~ "اعثر على التأثير الذي تريده ، وفعل الإعداد باستخدام مربع التأكيد ، وانتق "
#~ "أي طريقة تريد أن يتأثر بها الإعداد و بأي قيمة."

#~ msgid "Consult the documentation for more details."
#~ msgstr "للمزيد من التفاصيل راجع التوثيق."

#~ msgid "Edit Shortcut"
#~ msgstr "حرّر الاختصار"

#~ msgid "0123456789-+,xX:"
#~ msgstr "0123456789-+,xX:"

#~ msgid "&Desktop"
#~ msgstr "&سطح المكتب"

#~ msgid "Smart"
#~ msgstr "ذكي"

#~ msgid "kcmkwinrules"
#~ msgstr "kcmkwinrules"

#~ msgid "Opaque"
#~ msgstr "معتم"

#~ msgid "Transparent"
#~ msgstr "شفاف"

#~ msgid "&Moving/resizing"
#~ msgstr "ح&رّك/أعد تحجيم"

#, fuzzy
#~| msgid "Title:"
#~ msgid "Tiled"
#~ msgstr "العنوان:"

#~ msgid ""
#~ "For selecting all windows belonging to a specific application, selecting "
#~ "only window class should usually work."
#~ msgstr ""
#~ "لاختيار كلّ النوافذ المرتبطة بتطبيق محدّد ، اختر فقط صنف النافذة الذي يفترض "
#~ "أن يعمل."

#~ msgid "Use window &class (whole application)"
#~ msgstr "استخدم ص&نف النافذة (تطبيق كامل)"

#~ msgid ""
#~ "For selecting a specific window in an application, both window class and "
#~ "window role should be selected. Window class will determine the "
#~ "application, and window role the specific window in the application; many "
#~ "applications do not provide useful window roles though."
#~ msgstr ""
#~ "لاختيار نافذة محدّدة من تطبيق، يجب اختيار كل من صنف النافذة ودورها. صنف "
#~ "النافذة سيحدّد التطبيق, ودور النافذة  نافذةَ التطبيق؛ لكن الكثير من "
#~ "التطبيقات لا يوفر أدوارا مفيدة."

#~ msgid "Use window class and window &role (specific window)"
#~ msgstr " استخدم ص&نف النافذة ودور النافذة (نافذة محدّدة)"

#~ msgid ""
#~ "With some (non-KDE) applications whole window class can be sufficient for "
#~ "selecting a specific window in an application, as they set whole window "
#~ "class to contain both application and window role."
#~ msgstr ""
#~ "مع بعض التطبيقات (ليس ضمن كِيدِي) ، يكفي صنف النافذة لاختيار نافذة محددة في "
#~ "التطبيق ؛ حيث أنهم يضعون كامل صنف النافذة ليحوي التطبيق و دور النافذة."

#~ msgid "Use &whole window class (specific window)"
#~ msgstr "استخدم ص&نف النافذة الكامل (نافذة محدّدة)"

#~ msgid "Match also window &title"
#~ msgstr "طابق أيضاً ع&نوان النافذة"

#~ msgid "Extra role:"
#~ msgstr "دور إضافي:"

#~ msgid "Window &Extra"
#~ msgstr "نافذة إ&ضافية"

#~ msgid "&Geometry"
#~ msgstr "ال&هندسة"

#~ msgid "&Preferences"
#~ msgstr "ال&تفضيلات"

#~ msgid "&No border"
#~ msgstr "&بدون حدود"

#~ msgid "0123456789"
#~ msgstr "0123456789"

#~ msgid "W&orkarounds"
#~ msgstr "ت&حايلات"

#~ msgid "Strictly obey geometry"
#~ msgstr "التزم بصرامة للأبعاد"
